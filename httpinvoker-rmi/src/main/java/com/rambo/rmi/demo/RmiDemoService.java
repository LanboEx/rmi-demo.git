package com.rambo.rmi.demo;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Rambo
 */
public interface RmiDemoService extends Remote {


    /**
     * Rmi 测试方法获取用户名称
     *
     * @param name 用户名称
     * @return 用户名称
     */
    String getUserName(String name) throws RemoteException;
}
