package com.rambo.rmi.demo.impl;

import com.rambo.rmi.demo.RmiDemoService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;

/**
 * RMi 测试实现类
 *
 * @author Rambo 2018-09-17
 **/
public class RmiDemoServiceImpl implements RmiDemoService {

     RmiDemoServiceImpl() throws RemoteException {
        UnicastRemoteObject.exportObject(this, 0);
    }

    @Override
    public String getUserName(String name) {

        Random random = new Random();
        return name + random.nextLong();
    }
}