package com.rambo.httpinvoker.server.impl;

import com.rambo.httpinvoker.api.UserService;
import com.rambo.httpinvoker.api.bo.User;

/**
 * 用户服务实现类
 *
 * @author Rambo 2018-09-26
 **/
public class UserServiceImpl implements UserService {

    @Override
    public User getUserById(String uuid) {
        User user = new User();
        user.setUuid(uuid);
        user.setName("Orson");
        user.setPasswd("xyxy");
        user.setSex("F");
        user.setPhone("13974856211");
        user.setPhoto("/photo/user/xyxy.gif");
        user.setEmail("954875698@qq.com");
        user.setCreateBy("orson");
        return user;
    }
}