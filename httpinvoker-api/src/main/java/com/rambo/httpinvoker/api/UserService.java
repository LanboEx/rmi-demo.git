package com.rambo.httpinvoker.api;

import com.rambo.httpinvoker.api.bo.User;

/**
 * 用户服务接口类
 * @author Rambo
 */
public interface UserService {


    /**
     * 通过ID获取用户
     *
     * @param uuid 用户ID
     * @return 用户实体
     */
    User getUserById(String uuid);
}
